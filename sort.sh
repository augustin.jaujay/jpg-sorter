#!/bin/bash

DIRECTORY_TO_SORT=$1
dateRegexp="([0-9]{4}):([0-9]{2}):([0-9]{2})"
imgRegexp=".*\.(jpeg|jpg|JPG|heic|HEIC)$"
heicRegexp=".*\.(heic|HEIC)$"

window_width=$(tput cols)
bar_size=$(($window_width-10))
bar_char_done="#"
bar_char_todo="-"
bar_percentage_scale=2

progress_bar_displayed=false

function show_progress {
    current="$1"
    total="$2"

    # calculate the progress in percentage 
    percent=$(bc <<< "scale=$bar_percentage_scale; 100 * $current / $total")
    # The number of done and todo characters
    done=$(bc <<< "scale=0; $bar_size * $percent / 100" )
    todo=$(bc <<< "scale=0; $bar_size - $done" )

    # build the done and todo sub-bars
    done_sub_bar=$(printf "%${done}s" | tr " " "${bar_char_done}")
    todo_sub_bar=$(printf "%${todo}s" | tr " " "${bar_char_todo}")

    # output the bar
    if $progress_bar_displayed
    then
        echo -ne "\r[${done_sub_bar}${todo_sub_bar}] ${percent}%"
    else 
        echo -ne "[${done_sub_bar}${todo_sub_bar}] ${percent}%"
    fi
    progress_bar_displayed=true

    if [ $total -eq $current ]; then
        echo -e "\nDONE"
    fi
}

function log {
    if $progress_bar_displayed
    then
        clear_last_line=$(printf "%${window_width}s" | tr "*" " ")
        echo -ne "\r$clear_last_line"
        echo -e "\r$@"
    else 
        echo "$@"
    fi
    progress_bar_displayed=false
}


if [ -z $DIRECTORY_TO_SORT ]
then  
    log "Error : no directory to sort provided."
elif [ ! -d $DIRECTORY_TO_SORT ]
then
    log "Error : directory $DIRECTORY_TO_SORT does not exists."
else
    cd $DIRECTORY_TO_SORT
    donetasks="0"
    totaltasks=`ls | wc -l`
    
    for image in `ls`; do
        if [[ $image =~ $imgRegexp ]]
        then
            if [[ $image =~ $heicRegexp ]]
            then 
                EXIF_DATE=`exiftool -T -DateTimeOriginal $image`
            else
                # EXIF_DATE=`identify -verbose $image | grep exif:DateTime: | grep -oE $dateRegexp`
                EXIF_DATE=`exif -t=DateTimeOriginal $image | grep Value`
            fi
            if [[ $EXIF_DATE =~ $dateRegexp ]]
            then
                DEST_DIR="${BASH_REMATCH[1]}/${BASH_REMATCH[2]}" # Destination folder is Year/Month
                if [ ! -d $DEST_DIR ]
                then
                    log "Creating directory $DEST_DIR."
                    mkdir -p $DEST_DIR
                fi
                log $(mv -v $image $DEST_DIR)
            else
                log "No EXIF data found for file $image."
            fi
        fi

        donetasks=$(bc <<< "$donetasks + 1")
        # show_progress $donetasks $totaltasks
    done 
fi